package it.ildevid.spindoxtest

import android.app.Application
import android.content.Context

class SpindoxTestApplication: Application() {

    companion object {
        lateinit var myAppContext: Context
    }

    override fun onCreate() {
        super.onCreate()
        myAppContext = applicationContext
    }

}