package it.ildevid.spindoxtest.model

import android.net.Uri
import java.io.Serializable

data class BeerModel(
    val id: Int,
    val imageUrl: Uri? = null,
    val name: String = "",
    val description: String? = null,
    var alcoholContent: String? = null,
    var ibu: String? = null,
    val firstBrewed: String? = null,
    var foodPairing: List<String> = listOf(),
    val brewerTips: String? = null,
    var listItemQueue: Boolean = false
) : Serializable {

    fun getFoodPairingFormatted(): String {
        return foodPairing.joinToString(separator = ",\n")
    }

}
