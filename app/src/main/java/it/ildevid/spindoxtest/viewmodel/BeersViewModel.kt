package it.ildevid.spindoxtest.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import it.ildevid.spindoxtest.adapter.BeerListAdapter
import it.ildevid.spindoxtest.model.BeerModel
import it.ildevid.spindoxtest.network.CoreAPI
import it.ildevid.spindoxtest.network.getBeerList
import it.ildevid.spindoxtest.network.utils.log
import it.ildevid.spindoxtest.network.utils.loge
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch

class BeersViewModel : ViewModel() {

    private val serverApi = CoreAPI().getServerAPI()

    var adapter: BeerListAdapter? = null
    var loadingBeers = MutableLiveData(false)

    var page: Int = 1

    val beerTextFilter = MutableLiveData("")
    var oldFilter = ""

    fun loadBeers(onFinish: ((List<BeerModel>) -> Unit)) {
        try {
            viewModelScope.launch {
                val response =
                    serverApi.getBeerListPaginated(pageIndex = page)
                log("GetBeerList size = ${response.size}")

                val beerList = getBeerList(response, adapter?.getItems())

                if (isActive) onFinish.invoke(beerList)
            }
        } catch (e: Exception) {
            loge("Error load beers", e)
            onFinish.invoke(adapter?.getItems()?.toList()?.let { ArrayList(it) } ?: listOf())
        }
    }

}