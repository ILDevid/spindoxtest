package it.ildevid.spindoxtest.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.GenericTransitionOptions
import com.bumptech.glide.Glide
import it.ildevid.spindoxtest.R
import it.ildevid.spindoxtest.databinding.FragmentBeerDetailBinding
import it.ildevid.spindoxtest.model.BeerModel
import it.ildevid.spindoxtest.widget.ToolbarSimple

class BeerDetailFragment : Fragment() {

    private lateinit var binding: FragmentBeerDetailBinding

    private val args: BeerDetailFragmentArgs by navArgs()
    private lateinit var beer: BeerModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentBeerDetailBinding.inflate(inflater, container, false)
        beer = args.beerItem
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.lifecycleOwner = viewLifecycleOwner
        binding.beer = beer

        binding.toolbar.setToolbarListener(object : ToolbarSimple.ToolbarListener {
            override fun onBackPressed(v: View?) {
                findNavController().popBackStack()
            }
        })

        Glide.with(binding.imageBeer)
            .load(beer.imageUrl)
            .transition(GenericTransitionOptions.with(R.anim.zoom_in))
            .error(R.drawable.beer_placeholder)
            .into(binding.imageBeer)

        binding.cardBottom.startAnimation(
            AnimationUtils.loadAnimation(
                requireContext(),
                R.anim.slide_up
            )
        )
    }

}