package it.ildevid.spindoxtest.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import it.ildevid.spindoxtest.R
import it.ildevid.spindoxtest.adapter.BeerListAdapter
import it.ildevid.spindoxtest.databinding.FragmentBeerListBinding
import it.ildevid.spindoxtest.model.BeerModel
import it.ildevid.spindoxtest.network.utils.log
import it.ildevid.spindoxtest.network.utils.navigate
import it.ildevid.spindoxtest.viewmodel.BeersViewModel
import it.ildevid.spindoxtest.widget.ToolbarSimple
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class BeerListFragment : Fragment(), BeerListAdapter.BeerInteractionListener {

    private lateinit var binding: FragmentBeerListBinding

    private val model: BeersViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentBeerListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.lifecycleOwner = viewLifecycleOwner
        binding.model = model

        binding.toolbar.setToolbarListener(object : ToolbarSimple.ToolbarListener {
            override fun onBackPressed(v: View?) {
                activity?.finish()
            }
        })

        model.loadingBeers.observe(viewLifecycleOwner) { loading ->
            if (!loading) {
                lifecycleScope.launch {
                    delay(750)
                    binding.progressView.visibility = INVISIBLE
                }
            } else {
                binding.progressView.visibility = VISIBLE
            }
        }

        model.beerTextFilter.observe(viewLifecycleOwner) { filter ->

            val adapter = binding.recyclerViewBeers.adapter as? BeerListAdapter

            if (model.oldFilter.isNotEmpty() && filter.isNullOrEmpty()) {
                addScrollListener()
                adapter?.recoverBackup()
            } else if (filter.isNotEmpty()) {
                binding.recyclerViewBeers.clearOnScrollListeners()
            }
            model.oldFilter = filter ?: ""

            adapter?.let {
                if (filter.isNotEmpty()) {
                    model.loadingBeers.value = true
                }
                adapter.filter.filter(filter) {
                    log("Filter size is $it")
                    model.loadingBeers.value = false
                }
            }
        }

        binding.recyclerViewBeers.layoutManager = LinearLayoutManager(requireContext())
        if (model.adapter == null) model.adapter = BeerListAdapter(ArrayList(), ArrayList(), this)
        binding.recyclerViewBeers.adapter = model.adapter
        addScrollListener()

        if (model.adapter == null || model.adapter!!.itemCount == 0) loadBeers(cancel = true)
    }

    @Synchronized
    fun loadBeers(cancel: Boolean) {
        if (!cancel && model.loadingBeers.value == true) return

        val adapter = binding.recyclerViewBeers.adapter as BeerListAdapter
        if (cancel) {
            model.page = 1
        }

        binding.recyclerViewBeers.clearOnScrollListeners()

        lifecycleScope.launch {

            model.loadingBeers.value = true
            binding.recyclerViewBeers.post {

                val lastPageItemIndex = if (adapter.itemCount > 0) {
                    adapter.itemCount - 1
                } else {
                    RecyclerView.NO_POSITION
                }

                model.loadBeers { beers ->
                    if (beers.isNotEmpty()) {
                        adapter.updateItems(beers) {
                            model.page++
                            handleScroll()
                            adapter.updateListBackup(beers)
                            model.loadingBeers.value = false

                            if (lastPageItemIndex != RecyclerView.NO_POSITION) adapter.notifyItemChanged(
                                lastPageItemIndex
                            )
                        }
                    } else {
                        if (adapter.itemCount > 0) {
                            Toast.makeText(
                                context,
                                getString(R.string.no_more_beers),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                        model.loadingBeers.value = false
                    }
                    addScrollListener()
                }
            }

        }
    }

    private fun addScrollListener() {
        binding.recyclerViewBeers.apply {
            clearOnScrollListeners()
            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    handleScroll()
                }
            })
        }
    }

    private fun handleScroll() {
        binding.recyclerViewBeers.apply {
            if (adapter!!.itemCount > 0 && (layoutManager as? LinearLayoutManager)?.findLastCompletelyVisibleItemPosition()!! >= adapter!!.itemCount - 1) {
                log("fine lista")
                loadBeers(cancel = false)
            }
        }
    }

    override fun onBeerClick(beer: BeerModel) {
        if (model.loadingBeers.value == false) {
            binding.editTextSearch.setText("")
            navigate(
                BeerListFragmentDirections.actionBeerListFragmentToBeerDetailFragment(
                    beer
                )
            )
        }
    }

}