package it.ildevid.spindoxtest.widget

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.animation.OvershootInterpolator
import android.widget.LinearLayout
import it.ildevid.spindoxtest.R
import it.ildevid.spindoxtest.databinding.WidgetToolbarSimpleBinding

class ToolbarSimple(context: Context?, attrs: AttributeSet?) : LinearLayout(context, attrs) {

	private val binding: WidgetToolbarSimpleBinding =
		WidgetToolbarSimpleBinding.inflate(LayoutInflater.from(context), this, true)

	interface ToolbarListener {
		fun onBackPressed(v: View?)
	}

	init {
		val a = context?.obtainStyledAttributes(attrs, R.styleable.ToolbarSimple)
		var animateTitle = false
		if (a != null) {
			binding.imageBack.visibility = if (a.getBoolean(
					R.styleable.ToolbarSimple_backVisible,
					false
				)
			) VISIBLE else INVISIBLE
			binding.textTitle.text = a.getString(R.styleable.ToolbarSimple_title) ?: ""
			animateTitle = a.getBoolean(R.styleable.ToolbarSimple_animateTitle, false)
		}
		a?.recycle()
		if (animateTitle) {
			post {
				binding.textTitle.alpha = 0f
				binding.textTitle.postDelayed({ animateTitleEntrance() }, 350)
			}
		}
	}

	fun setTitle(title: String?) {
		binding.textTitle.text = title
	}

	fun setToolbarListener(toolbarListener: ToolbarListener?) {
		binding.toolbarListener = toolbarListener
	}

	private fun animateTitleEntrance() {
		binding.textTitle.animate()
			.translationXBy(-150f)
			.setDuration(0)
			.start()

		binding.textTitle.animate()
			.translationXBy(150f)
			.alpha(1f)
			.setDuration(300)
			.setInterpolator(OvershootInterpolator(3.5f))
			.start()
	}

}