package it.ildevid.spindoxtest.network

import it.ildevid.spindoxtest.BASE_URL
import it.ildevid.spindoxtest.BuildConfig
import it.ildevid.spindoxtest.TIMEOUT
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.Executor
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

class CoreAPI {

    private var _serverApi: BeerCoreApi? = null

    private val _cpuCount = Runtime.getRuntime().availableProcessors()
    private val _corePoolSize = _cpuCount + 1
    private val _threadPoolExecutor: Executor = Executors.newFixedThreadPool(_corePoolSize)

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .client(getClient())
            .baseUrl(BASE_URL)
            .callbackExecutor(_threadPoolExecutor)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun getClient(): OkHttpClient {
        val builder: OkHttpClient.Builder = OkHttpClient.Builder()
            .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(TIMEOUT, TimeUnit.SECONDS)
        if (BuildConfig.DEBUG) {
            val interceptor = HttpLoggingInterceptor()
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
            builder.addInterceptor(interceptor)
        }
        return builder.build()
    }


    fun getServerAPI(): BeerCoreApi {
        if (_serverApi == null) {
            _serverApi = CoreAPI().getRetrofit().create(BeerCoreApi::class.java)
        }
        return _serverApi!!
    }

}