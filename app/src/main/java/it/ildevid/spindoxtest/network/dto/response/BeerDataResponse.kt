package it.ildevid.spindoxtest.network.dto.response

data class BeerDataResponse(
    val id: Int,
    val image_url: String? = null,
    val name: String? = null,
    val description: String? = null,
    var abv: String? = null,
    var ibu: String? = null,
    val first_brewed: String? = null,
    var food_pairing: List<String>? = null,
    val brewers_tips: String? = null
)
