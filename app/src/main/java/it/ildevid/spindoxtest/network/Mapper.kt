package it.ildevid.spindoxtest.network

import android.net.Uri
import it.ildevid.spindoxtest.R
import it.ildevid.spindoxtest.SpindoxTestApplication.Companion.myAppContext
import it.ildevid.spindoxtest.model.BeerModel
import it.ildevid.spindoxtest.network.dto.response.BeerDataResponse

fun getBeerList(
    response: List<BeerDataResponse>,
    cachedBeers: List<BeerModel>? = null
): List<BeerModel> {
    val beerList = ArrayList<BeerModel>()

    cachedBeers?.let {
        it.firstOrNull()?.listItemQueue = false
        it.lastOrNull()?.listItemQueue = false
        beerList.addAll(it.toList())
    }

    response.forEach { beerData ->
        val imageUrl = try {
            Uri.parse(beerData.image_url)
        } catch (e: Exception) {
            null
        }
        val alcoholContent = if (!beerData.abv.isNullOrEmpty()) {
            "${beerData.abv}%"
        } else {
            null
        }
        val ibu = try {
            beerData.ibu?.toDouble()?.toInt()?.toString()
        } catch (e: Exception) {
            null
        }
        val beer = BeerModel(
            id = beerData.id,
            imageUrl = imageUrl,
            name = beerData.name ?: "",
            description = beerData.description,
            alcoholContent = alcoholContent ?: myAppContext.getString(R.string.data_unavailable),
            ibu = ibu ?: myAppContext.getString(R.string.data_unavailable),
            firstBrewed = beerData.first_brewed,
            foodPairing = beerData.food_pairing ?: listOf(),
            brewerTips = beerData.brewers_tips
        )
        beerList.add(beer)
    }

    beerList.firstOrNull()?.listItemQueue = true
    beerList.lastOrNull()?.listItemQueue = true

    return beerList
}