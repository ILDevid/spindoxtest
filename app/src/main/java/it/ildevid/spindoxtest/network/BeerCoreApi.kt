package it.ildevid.spindoxtest.network

import it.ildevid.spindoxtest.network.dto.response.BeerDataResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface BeerCoreApi {

    @GET("beers")
    suspend fun getBeerListPaginated(
        @Query("page") pageIndex: Int,
        @Query("per_page") itemsPerPage: Int? = null
    ): List<BeerDataResponse>

}