package it.ildevid.spindoxtest.network.utils

import android.util.Log
import it.ildevid.spindoxtest.BuildConfig

fun Any.log(vararg messages: Any?) {
    if (BuildConfig.DEBUG) {
        val tag = this.javaClass.simpleName.ifEmpty { "App" }
        Log.d(tag, messages.joinToString("\t"))
    }
}

fun Any.loge(vararg messages: Any?) {
    if (BuildConfig.DEBUG) {
        val tag = this.javaClass.simpleName.ifEmpty { "App" }
        Log.e(tag, messages.joinToString("\t"))
        messages.filterIsInstance<Throwable>().forEach {
            it.printStackTrace()
        }
    }
}