package it.ildevid.spindoxtest.adapter

import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.core.content.ContextCompat
import androidx.core.widget.TextViewCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.google.android.material.shape.CornerFamily
import it.ildevid.spindoxtest.R
import it.ildevid.spindoxtest.SpindoxTestApplication.Companion.myAppContext
import it.ildevid.spindoxtest.databinding.RowBeerBinding
import it.ildevid.spindoxtest.model.BeerModel
import it.ildevid.spindoxtest.network.utils.log

class BeerListAdapter(
    private var mValues: ArrayList<BeerModel> = ArrayList(),
    private var mValuesBackup: ArrayList<BeerModel> = ArrayList(),
    private val listener: BeerInteractionListener
) : ListAdapter<BeerModel, BeerListAdapter.BeerViewHolder>(
    BeerDiffCallback()
), Filterable {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BeerListAdapter.BeerViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding: RowBeerBinding =
            RowBeerBinding.inflate(layoutInflater, parent, false)
        return BeerViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: BeerListAdapter.BeerViewHolder, position: Int) {
        holder.bind(
            mValues[position],
            position % 2 != 0,
            position == 0,
            position == mValues.size - 1,
            listener
        )
    }

    override fun getItemCount(): Int = mValues.size

    fun getItems() = mValues

    fun updateItems(beerListUpdated: List<BeerModel>, resultCallback: Runnable? = null) {
        mValues = ArrayList(beerListUpdated)
        submitList(mValues, resultCallback)
    }

    fun updateListBackup(beerListBackup: List<BeerModel>) {
        mValuesBackup = ArrayList(beerListBackup)
    }

    fun recoverBackup() {
        updateItems(mValuesBackup)
    }

    inner class BeerViewHolder(private val binding: RowBeerBinding) :
        RecyclerView.ViewHolder(binding.root) {

        private val context = myAppContext
        private var cornerSize = context.resources
            .getDimension(R.dimen.card_view_beer_corner_round)

        fun bind(
            beer: BeerModel,
            oddItem: Boolean,
            firstItem: Boolean,
            lastItem: Boolean,
            listener: BeerInteractionListener
        ) {

            binding.oddItem = oddItem
            binding.lastItem = lastItem
            binding.beer = beer
            binding.listener = listener

            Glide.with(binding.root)
                .load(beer.imageUrl)
                .transition(DrawableTransitionOptions.withCrossFade())
                .error(R.drawable.beer_placeholder)
                .into(binding.imageBeer)

            val drawableTintList = if (oddItem) {
                ColorStateList.valueOf(ContextCompat.getColor(myAppContext, R.color.grayTextLight))
            } else {
                ColorStateList.valueOf(ContextCompat.getColor(myAppContext, R.color.orangeLight_60))
            }
            TextViewCompat.setCompoundDrawableTintList(binding.textAlcohol, drawableTintList)
            TextViewCompat.setCompoundDrawableTintList(binding.textIbu, drawableTintList)

            if (beer.listItemQueue) {
                if (firstItem && !lastItem) {
                    binding.imageDividerBottom.visibility = View.VISIBLE
                    binding.cardBeer.shapeAppearanceModel = binding.cardBeer.shapeAppearanceModel
                        .toBuilder()
                        .setTopLeftCorner(CornerFamily.ROUNDED, cornerSize)
                        .setTopRightCorner(CornerFamily.ROUNDED, cornerSize)
                        .setBottomLeftCornerSize(0f)
                        .setBottomRightCornerSize(0f)
                        .build()
                } else if (lastItem && !firstItem) {
                    binding.imageDividerBottom.visibility = View.GONE
                    binding.cardBeer.shapeAppearanceModel = binding.cardBeer.shapeAppearanceModel
                        .toBuilder()
                        .setBottomLeftCorner(CornerFamily.ROUNDED, cornerSize)
                        .setBottomRightCorner(CornerFamily.ROUNDED, cornerSize)
                        .setTopLeftCornerSize(0f)
                        .setTopRightCornerSize(0f)
                        .build()
                } else if (firstItem) {
                    binding.imageDividerBottom.visibility = View.GONE
                    binding.cardBeer.shapeAppearanceModel = binding.cardBeer.shapeAppearanceModel
                        .toBuilder()
                        .setAllCorners(CornerFamily.ROUNDED, cornerSize)
                        .build()
                }
            } else {
                binding.imageDividerBottom.visibility = View.VISIBLE
                binding.cardBeer.shapeAppearanceModel = binding.cardBeer.shapeAppearanceModel
                    .toBuilder()
                    .setAllCorners(CornerFamily.ROUNDED, 0f)
                    .build()
            }

            binding.executePendingBindings()

        }

    }

    private class BeerDiffCallback : DiffUtil.ItemCallback<BeerModel>() {
        override fun areItemsTheSame(oldItem: BeerModel, newItem: BeerModel): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: BeerModel, newItem: BeerModel): Boolean {
            return oldItem.listItemQueue == newItem.listItemQueue
        }
    }

    interface BeerInteractionListener {
        fun onBeerClick(beer: BeerModel)
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(filter: CharSequence): FilterResults {
                val results = FilterResults()

                mValuesBackup.forEach { it.listItemQueue = false }
                mValuesBackup.firstOrNull()?.listItemQueue = true
                mValuesBackup.lastOrNull()?.listItemQueue = true

                val listFiltered = mValuesBackup.filter {
                    it.name.contains(filter, true) || it.description?.contains(filter, true) == true
                }

                listFiltered.forEach { it.listItemQueue = false }
                listFiltered.firstOrNull()?.listItemQueue = true
                listFiltered.lastOrNull()?.listItemQueue = true

                results.values = listFiltered
                results.count = listFiltered.size

                log("List filtered size = ${listFiltered.size}")

                return results
            }

            override fun publishResults(filter: CharSequence, results: FilterResults) {
                val listFiltered = results.values as ArrayList<BeerModel>
                updateItems(listFiltered) {
                    if (mValues.isNotEmpty()) {
                        notifyItemRangeChanged(0, mValues.size)
                    }
                }
            }
        }
    }

}